<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'imgur' => [
        'endpoint' => env('IMGUR_ENDPOINT', 'https://api.imgur.com/3/'),

        'clientId' => env('IMGUR_CLIENT_ID'),
        'clientSecret' => env('IMGUR_CLIENT_SECRET'),
    ],

];
