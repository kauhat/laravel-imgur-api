<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

use App\Services\ImgurApiService;

class GalleryController extends Controller
{
    protected $apiService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ImgurApiService $apiService)
    {
        // Inject the API service class.
        $this->apiService = $apiService;
    }

    /**
     * Display a tag gallery.
     *
     * @param  \Illuminate\Http\Request
     * @param  string $tag
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, string $tagName)
    {
        $validParams = $request->validate([
            'page' => ['integer'],
            'amountPerPage' => ['integer'],
        ]);

        // Get tag metadata.
        $meta = $this->apiService->getTagGalleryInfo($tagName);

        // Get current and amount per page.
        $currentPage = $validParams['page'] ?? 1;
        $amountPerPage = $validParams['amountPerPage'] ?? $meta['items_per_page'];

        // Get current start item.
        $startAt = ($currentPage - 1) * $amountPerPage;

        // Get rounded down page offset for API query.
        $pageOffset = (int) floor($currentPage * ($amountPerPage / $meta['items_per_page']));

        // Get tag items.
        [$items, $tag] = $this->apiService->getTagGalleryItems($tagName, $pageOffset - 1);

        // Get relative starting index.
        $startRel = min(0, $startAt - (($pageOffset)  * $meta['items_per_page']));

        // Filter returned items.
        $filtered = collect($items)
            ->skip($startRel)
            ->take($amountPerPage);

        // Construct paginator.
        $paginator = new LengthAwarePaginator($items, $tag['total_items'], $amountPerPage, $currentPage, [
            'path' => LengthAwarePaginator::resolveCurrentPath(),
        ]);

        // Append other parameters to paginator.
        $paginator->appends($request->input());

        return view('gallery.tag')
            ->with('items', $filtered)
            ->with('tag', $tag)
            ->with('paginator', $paginator);
    }
}
