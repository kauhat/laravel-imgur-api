<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Services\ImgurApiService;

class TagsController extends Controller
{
    protected $apiService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ImgurApiService $apiService)
    {
        // Inject the API service class.
        $this->apiService = $apiService;
    }

    /**
     * Get a list of default tags.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = $this->apiService->getTags();

        return view('tags.index')
            ->with('tags', $tags);
    }

    /**
     * Get info about a specific tag.
     *
     * @param  string  $tag
     * @return \Illuminate\Http\Response
     */
    public function show($tag)
    {
        return redirect()
            ->route('gallery.tag', $tag);
    }
}
