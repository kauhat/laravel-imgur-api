<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use Carbon\Carbon;

class GalleryItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // Todo: Build full mapper.

        // Lazy array conversion.
        $data = (array) $this->resource;

        // Convert and append date...
        $data['datetime'] = Carbon::createFromTimestamp($this->resource->datetime);

        return $data;
    }
}
