<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TagResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->resource->name,
            'display_name' => $this->resource->display_name,
            'total_items' => $this->resource->total_items,

            // Todo: Double check amount items per page.
            'items_per_page' => 60,
        ];
    }
}
