<?php

namespace App\Services;

use GuzzleHttp\Client;
use League\Uri\UriTemplate;
use Psr\Http\Message\ResponseInterface;

use App\Http\Resources\GalleryItemResource;
use App\Http\Resources\TagResource;

class ImgurApiService
{
    protected $apiClient;

    /**
     * Create a new service instance.
     *
     * @return void
     */
    public function __construct()
    {
        // Initialize the consumer client.
        $this->apiClient = new Client([
            // Base URI used with relative requests.
            'base_uri' => config('services.imgur.endpoint'),

            'headers' => [
                'Authorization' => 'Client-ID ' . config('services.imgur.clientId'),
            ],
        ]);
    }

    /**
     * Get a list of popular tags.
     *
     * @return Array
     */
    public function getTags(): Array
    {
        // Make request...
        $response = $this->apiClient
            ->request('GET', 'tags');

        // Process response body.
        $body = $this->processResponseBody($response);

        return $body->data->tags;
    }

    /**
     * Get data for a gallery of a supplied tag.
     *
     * Returns a tuple of gallery items and tag metadata.
     *
     * @param  string   $tag
     * @return Array
     */
    public function getTagGalleryItems(string $tag, int $page = 0, string $sort = 'time', string $window = 'day'): Array
    {
        // Build URI path from template.
        $template = new UriTemplate('gallery/t/{tag}/{sort}/{window}/{page}');
        $path = $template->expand([
            'tag' => $tag,
            'sort' => $sort,
            'window' => $window,
            'page' => $page,
        ])->getPath();

        // Make request.
        $response = $this->apiClient
            ->request('GET', $path, [
                'query' => ['album_previews' => true],
            ]);

        //
        $body = $this->processResponseBody($response);
        $data = ($body->data);

        // Transform response JSON with resource mapper.
        $tag = TagResource::make($data)
            ->resolve();

        // Transform tag items.
        $items = GalleryItemResource::collection($data->items ?? [])
            ->resolve();

        return [$items, $tag];
    }

    /**
     * Get metadata for a tag gallery.
     *
     * @param  string   $tag
     * @return Array
     */
    public function getTagGalleryInfo(string $tag): Array
    {
        // Build URI path from template.
        $template = new UriTemplate('gallery/tag_info/{tag}');
        $path = $template->expand([
            'tag' => $tag,
        ])->getPath();

        // Make request.
        $response = $this->apiClient
            ->request('GET', $path, [
                'query' => ['album_previews' => true],
            ]);

        //
        $body = $this->processResponseBody($response);
        $data = ($body->data);

        return TagResource::make($data)
            ->resolve();
    }

    /**
     * Decode response JSON and do some very basic validation.
     *
     * @param  ResponseInterface $response
     * @return stdClass
     */
    protected function processResponseBody(ResponseInterface $response)
    {
        //
        $responseContents = $response->getBody()
            ->getContents();

        // Decode returned JSON.
        $responseData = json_decode($responseContents);

        // Basic error checking.
        if (empty($responseData) || $responseData->success !== true) {
            // Handle thjs later.
            throw new \RuntimeException;
        }

        return $responseData;
    }
}
