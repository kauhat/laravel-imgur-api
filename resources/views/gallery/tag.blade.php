@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Items for tag {{ $tag['display_name'] }}</h1>

    @dump($tag)

    @empty($items)
        <p>This gallery contains no items.</p>
    @else
        <p>Displaying {{ count($items) }} items of {{ $tag['total_items'] }}.

        <p>
            <a href="{{ route('gallery.tag', ['tag' => $tag['name'], 'amountPerPage' => 10]) }}">10</a>
            <a href="{{ route('gallery.tag', ['tag' => $tag['name'], 'amountPerPage' => 20]) }}">20</a>
            <a href="{{ route('gallery.tag', ['tag' => $tag['name'], 'amountPerPage' => 30]) }}">30</a>
            <a href="{{ route('gallery.tag', ['tag' => $tag['name'], 'amountPerPage' => 60]) }}">60</a>
        </p>

        @include('partials.gallery-items', ['items' => $items])
    @endempty

    {{ $paginator->links() }}
</div>
@endsection
