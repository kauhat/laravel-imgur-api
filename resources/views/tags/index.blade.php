@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Popular Tags</h1>

    <ul>
        @foreach ($tags as $tag)
            <li>
                <a href="{{ route('gallery.tag', $tag->name) }}">
                    {{ $tag->display_name }}
                </a>
            </li>
        @endforeach
    </ul>
</div>
@endsection
