<div class="card">
    @if(!empty($item['images']) && !$item['images'][0]->animated)
        <img src="{{ $item['images'][0]->link ?? '' }}" class="card-img-top">
    @endif

    <div class="card-body">
        <a href="{{ $item['link'] }}" class="stretched-link">
            {{ $item['title'] }}
        </a>
    </div>

    <div class="card-footer">
      <small class="text-muted">Posted {{ $item['datetime']->diffForHumans() }}</small>
      <span class="badge badge-primary">Album</span>
    </div>
</div>
