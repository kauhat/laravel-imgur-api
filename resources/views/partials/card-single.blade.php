<div class="card">
    @if(!$item['animated'] ?? false)
        <img src="{{ $item['link'] ?? '' }}" class="card-img-top">
    @endif

    <div class="card-body">
        <a href="{{ $item['link'] }}" class="stretched-link">
            {{ $item['title'] }}
        </a>
    </div>

    <div class="card-footer">
      <small class="text-muted">Posted {{ $item['datetime']->diffForHumans() }}</small>
    </div>
</div>
