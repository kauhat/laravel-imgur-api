<div class="row">
    @foreach ($items as $item)
        <div class="col-sm-4 mb-3">
            @if($item['is_album'])
                @include('partials.card-album', ['item' => $item])
            @else
                @include('partials.card-single', ['item' => $item])
            @endif
        </div>
    @endforeach
</div>
