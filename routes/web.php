<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('tags.index');
});

Route::prefix('tags')
    ->name('tags.')
    ->group(function () {
        Route::get('/', 'TagsController@index')
            ->name('index');

        Route::get('/{tag}', 'TagsController@show')
            ->name('show');
    });

Route::prefix('gallery')
    ->name('gallery.')
    ->group(function () {
        Route::get('/{tag}', 'GalleryController@show')
            ->name('tag');

        // Todo: Add trending, subreddit galleries, etc.
    });
