# Laravel Imgur API consumer

![gallery items](public/cats-items.png "List of tag gallery items")

![gallery pagination](public/cats-pagination.png "Gallery pagination")

## Setup

Copy .env.example and set `IMGUR_CLIENT_ID`. `IMGUR_CLIENT_SECRET` is not
needed for public access.

Run using builtin PHP server using:

```bash
php artisan serve
```

...or with Vessel Docker dev environment:

```bash
./vessel start
```

See more: https://vessel.shippingdocker.com/docs/get-started/

## Features

* `/tags` - Display a list of gallery tags.
* `/gallery/cats` - Display tag gallery items.

* Uses service class for API requests. (`app/Services/ImgurServiceApi`)
* Uses JsonResources to transform API responses into known maps. (`app/Http/Resources`)
* Basic pagination with ability to select amount per page.

## Todo...

* Move pagination code out of Controller class.
* Investigate pagination display amount offset issues.
* Handling of failed responses in service class.
* Validation of client requests (pagination is within range etc).
* Expand resource mapping
    * Replace lazy stcClass to array conversion.
    * Album images are still objects. (Create image resource?)
* Fix errors when viewing pages near end of large galleries. (Out of range?)
* Use multiple API calls to return more than 60 items.
